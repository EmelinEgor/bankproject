package com.bank;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceTransactionManagerAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;

import javax.sql.DataSource;
import java.util.Properties;

/**
 * Класс, содержащий метод main, который создает контекст и вызывает консоль.
 *
 * @see com.bank.api.AuthRest
 * @see com.bank.api.BillsRest
 * @see com.bank.api.CustomBasicAuthenticationEntryPoint
 * @see com.bank.model.entities.BankAccountEntity
 * @see com.bank.model.entities.BankAccountStatementEntity
 * @see com.bank.model.entities.RelationsEntity
 * @see com.bank.model.entities.UsersEntity
 * @see com.bank.model.data.AuthorizationData
 * @see com.bank.model.data.BankAccount
 * @see com.bank.utils.ValidHelper
 * @see com.bank.dao.impl.BillsDaoImpl
 * @see com.bank.dao.impl.BillsHistoryDaoImpl
 * @see com.bank.dao.impl.relationsDaoImpl
 * @see com.bank.dao.impl.UsersDaoImpl
 * @see com.bank.adapter.service.impl.AuthServiceImpl
 * @see com.bank.adapter.service.impl.BillsServiceImpl
 * @see com.bank.SecurityConfiguration
 */
@SpringBootApplication
@EnableAutoConfiguration(exclude = { //
        DataSourceAutoConfiguration.class, //
        DataSourceTransactionManagerAutoConfiguration.class, //
        HibernateJpaAutoConfiguration.class})
public class Application {

    @Autowired
    private Environment env;

    /**
     * Метод для настройки подключения к базе данных
     */
    @Bean(name = "dataSource")
    public DataSource getDataSource() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(env.getProperty("spring.datasource.driver-class-name"));
        dataSource.setUrl(env.getProperty("spring.datasource.url"));
        dataSource.setUsername(env.getProperty("spring.datasource.username"));
        dataSource.setPassword(env.getProperty("spring.datasource.password"));
        System.out.println("## getDataSource: " + dataSource);
        return dataSource;
    }

    /**
     * Настройка SessionFactory и создание бина для полседующего использования для получения session
     */
    @Bean(name = "sessionFactory")
    public SessionFactory getSessionFactory(DataSource dataSource) throws Exception {
        Properties properties = new Properties();

        properties.put("hibernate.dialect", env.getProperty("spring.jpa.properties.hibernate.dialect"));
        properties.put("hibernate.show_sql", env.getProperty("spring.jpa.show-sql"));
        properties.put("current_session_context_class", env.getProperty("spring.jpa.properties.hibernate.current_session_context_class"));
        properties.put("hibernate.temp.use_jdbc_metadata_defaults", false);

        LocalSessionFactoryBean factoryBean = new LocalSessionFactoryBean();

        factoryBean.setPackagesToScan("com.bank.model.entities");
        factoryBean.setDataSource(dataSource);
        factoryBean.setHibernateProperties(properties);
        factoryBean.afterPropertiesSet();

        SessionFactory sf = factoryBean.getObject();
        System.out.println("## getSessionFactory: " + sf);
        return sf;
    }

    /**
     * Привязывает transactionManager к sessionFactory для обработки аннотации @Transactional
     */
    @Bean(name = "transactionManager")
    public HibernateTransactionManager getTransactionManager(SessionFactory sessionFactory) {
        return new HibernateTransactionManager(sessionFactory);
    }


    public static void main(String[] args) throws Exception {
        SpringApplication.run(Application.class, args);
    }
}
