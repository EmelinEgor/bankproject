package com.bank.dao.impl;

import com.bank.dao.interfaces.UsersDao;
import com.bank.model.entities.UsersEntity;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Класс выполняющий операции работы сбазой данных с таблицей users
 * <br> Поля: {@link #sessionFactory}
 * <br> Методы: {@link #getUser(long)}
 *
 * @see com.bank.Application
 */
@Repository
@Transactional
public class UsersDaoImpl implements UsersDao {
    /**
     * Объект, позволяющий воздавать сессии, с помощью которых выполняются все операции с базой данных
     */
    private SessionFactory sessionFactory;
    /**
     * Создает новый объект
     *
     * @param sessionFactory инжекция зависимости, объект singleton
     */
    public UsersDaoImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }


    /**
     * Метод, возвращающий все карты, принадлежащие данному пользователю
     * @param card_number номер карты
     * @return Список всех карт
     */
    @Override
    public UsersEntity getUser(long card_number) throws HibernateException {
        Session session = sessionFactory.getCurrentSession();
        String hql = "from UsersEntity where card_number = :paramNumber";
        Query query = session.createQuery(hql, UsersEntity.class);
        query.setParameter("paramNumber", card_number);
        return query.getResultList().size() != 0 ? (UsersEntity) query.getResultList().get(0) : null;
    }

}
