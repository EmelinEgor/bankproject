package com.bank.dao.impl;

import com.bank.model.entities.AtmEntity;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public class AtmDaoImpl {

    private SessionFactory sessionFactory;

    public AtmDaoImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public AtmEntity findByAtmName (String name) throws HibernateException {
        Session session = sessionFactory.getCurrentSession();
        String hql = "from AtmEntity where atm_name = :paramName";
        Query query = session.createQuery(hql, AtmEntity.class);
        query.setParameter("paramName", name);
        return query.getResultList().size() != 0 ? (AtmEntity) query.getResultList().get(0) : null;
    }
}
