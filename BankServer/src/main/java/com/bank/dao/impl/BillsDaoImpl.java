package com.bank.dao.impl;

import com.bank.dao.interfaces.BillsDao;
import com.bank.model.entities.BankAccountEntity;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * Класс выполняющий операции работы сбазой данных с таблицей bills
 * <br> Поля: {@link #sessionFactory}
 * <br> Методы: {@link #getBills(int...)}, {@link #getBill(String, int[])}, {@link #updateBill(BankAccountEntity)}
 *
 * @see com.bank.Application
 */
@Repository
@Transactional
public class BillsDaoImpl implements BillsDao {

    /**
     * Объект, позволяющий воздавать сессии, с помощью которых выполняются все операции с базой данных
     */
    private SessionFactory sessionFactory;

    /**
     * Создает новый объект
     *
     * @param sessionFactory инжекция зависимости, объект singleton
     */
    public BillsDaoImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    /**
     * Метод, возвращающий список счетов, со всеми данными по их номерам
     *
     * @param billsNumber номера счетов
     * @return Список счетов со всеми данными
     */
    public List<BankAccountEntity> getBills(int... billsNumber) throws HibernateException{
        Session session = sessionFactory.getCurrentSession();
        String hql = "from BankAccountEntity where bill_number = :paramBill";
        Query query = session.createQuery(hql, BankAccountEntity.class);
        List<BankAccountEntity> billsEntities = new ArrayList<>();
        for (int x : billsNumber) {
            query.setParameter("paramBill", x);
            billsEntities.add((BankAccountEntity) query.getResultList().get(0));
        }
        return billsEntities;
    }

    /**
     * Метод, возвращающий номер счета с которым будем работать по совпадению номера счета и соответствующей ему валюты
     *
     * @param currency    тип валюты
     * @param billsNumber номера счета
     * @return Номер счета с которым будем работать
     */
    public int getBill(String currency, int[] billsNumber) throws HibernateException{
        Session session = sessionFactory.getCurrentSession();
        String hql = "from BankAccountEntity where bill_number = :paramBill AND currency = :paramCurrency";
        Query query = session.createQuery(hql, BankAccountEntity.class);
        int bill = -1;
        for (int x : billsNumber) {
            query.setParameter("paramBill", x);
            query.setParameter("paramCurrency", currency);
            if (query.getResultList().size() != 0) {
                bill = x;
            }
        }
        return bill;
    }

    /**
     * Метод, выполняющий обновление состояния счета в базе данных
     *
     * @param bankAccountEntity сущность счета, содержащая все данные
     */
    public void updateBill(BankAccountEntity bankAccountEntity) throws HibernateException {
        Session session = sessionFactory.getCurrentSession();
        session.saveOrUpdate("BillsEntity", bankAccountEntity);
    }
}
