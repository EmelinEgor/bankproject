package com.bank.dao.impl;

import com.bank.dao.interfaces.BillsHistoryDao;
import com.bank.model.entities.BankAccountStatementEntity;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.util.List;

/**
 * Класс выполняющий операции работы сбазой данных с таблицей transactions
 * <br> Поля: {@link #sessionFactory}
 * <br> Методы: {@link #createTransaction(int, int, String)}, {@link #getHistory(int)}
 *
 * @see com.bank.Application
 */
@Repository
@Transactional
public class BillsHistoryDaoImpl implements BillsHistoryDao {
    /**
     * Объект, позволяющий воздавать сессии, с помощью которых выполняются все операции с базой данных
     */
    private SessionFactory sessionFactory;

    /**
     * Создает новый объект
     *
     * @param sessionFactory инжекция зависимости, объект singleton
     */
    public BillsHistoryDaoImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    /**
     * Метод, создающий новую транзакцию, отражающую проведенную операцию со счетом
     * @param billNumber номер счета
     * @param amount количество наличности
     * @param type тип операции
     */
    @Override
    public void createTransaction(int billNumber, int amount, String type) throws HibernateException {
        Session session = sessionFactory.getCurrentSession();
        BankAccountStatementEntity bankAccountStatementEntity = new BankAccountStatementEntity();
        bankAccountStatementEntity.setSum(amount);
        bankAccountStatementEntity.setType(type);
        bankAccountStatementEntity.setDate(new Timestamp(new java.util.Date().getTime()));
        bankAccountStatementEntity.setBill_number(billNumber);
        session.save("BankAccountStatementEntity", bankAccountStatementEntity);
    }

    /**
     * Метод, возвращающий список всех транзакций
     * @param billNumber номер счета
     * @return Список все транзакций для текущего счета
     */
    @Override
    public List<BankAccountStatementEntity> getHistory(int billNumber) throws HibernateException {
        Session session = sessionFactory.getCurrentSession();
        String hql = "from BankAccountStatementEntity where bill_number = :paramBill";
        Query query = session.createQuery(hql, BankAccountStatementEntity.class);
        query.setParameter("paramBill", billNumber);
        return query.getResultList().size() != 0 ? query.getResultList() : null;
    }
}
