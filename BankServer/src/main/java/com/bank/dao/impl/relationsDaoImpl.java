package com.bank.dao.impl;

import com.bank.dao.interfaces.RelationsDao;
import com.bank.model.entities.RelationsEntity;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Класс выполняющий операции работы сбазой данных с таблицей relations
 * <br> Поля: {@link #sessionFactory}
 * <br> Методы: {@link #getBillsNumber(long)}
 *
 * @see com.bank.Application
 */
@Repository
@Transactional
public class relationsDaoImpl implements RelationsDao {
    /**
     * Объект, позволяющий воздавать сессии, с помощью которых выполняются все операции с базой данных
     */
    private SessionFactory sessionFactory;
    /**
     * Создает новый объект
     *
     * @param sessionFactory инжекция зависимости, объект singleton
     */
    public relationsDaoImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    /**
     * Метод, возвращающий список счетов, привязанных к данной карте
     * @param cardNumber номер карты
     * @return Массив номеров счетов
     */
    @Override
    public int[] getBillsNumber(long cardNumber) throws HibernateException {
        Session session = sessionFactory.getCurrentSession();
        String hql = "from RelationsEntity where card_number = :paramCard";
        Query query = session.createQuery(hql, RelationsEntity.class);
        query.setParameter("paramCard", cardNumber);
        List<RelationsEntity> relationsEntities = (List<RelationsEntity>) query.getResultList();
        int[] numbers = new int[relationsEntities.size()];
        for (int i = 0; i < relationsEntities.size(); i++) {
            numbers[i] = relationsEntities.get(i).getBill_number();
        }
        return numbers;
    }
}
