package com.bank.dao.interfaces;

import com.bank.model.entities.BankAccountStatementEntity;

import java.util.List;

public interface BillsHistoryDao {

    void createTransaction(int billNumber, int amount, String type);

    List getHistory(int billNumber);
}
