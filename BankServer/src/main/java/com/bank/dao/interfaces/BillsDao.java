package com.bank.dao.interfaces;

import com.bank.model.entities.BankAccountEntity;

import java.util.List;

public interface BillsDao {

    List<BankAccountEntity> getBills(int... billsNumber);

    int getBill(String currency, int[] billsNumber);

    void updateBill(BankAccountEntity bankAccountEntity);
}
