package com.bank.dao.interfaces;

public interface RelationsDao {
    int[] getBillsNumber(long cardNumber);
}
