package com.bank.dao.interfaces;

import com.bank.model.entities.UsersEntity;

import java.util.List;

public interface UsersDao {

    UsersEntity getUser(long card_number);
}
