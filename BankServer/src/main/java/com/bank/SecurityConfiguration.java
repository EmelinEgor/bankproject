package com.bank;

import com.bank.api.CustomBasicAuthenticationEntryPoint;
import com.bank.utils.MyAtmDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;

//
/**
 * Класс, конфигурирующий настройка Basic Authentication
 * <br> Методы: {@link #passwordEncoder()}, {@link #configureGlobalSecurity(AuthenticationManagerBuilder)},
 * {@link #configure(HttpSecurity)}, {@link #getBasicAuthEntryPoint()}
 *
 * @see com.bank.Application
 */
@Configuration
@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    @Autowired
    private MyAtmDetailsService userDetailsService;

    /**
     * Метод, возвращающий инстанс кодировщика, необходимого для кодирования пар логгин/пароль
     * @return инстанс кодировщика
     */
    @SuppressWarnings("deprecation")
    @Bean
    public static NoOpPasswordEncoder passwordEncoder() {
        return (NoOpPasswordEncoder) NoOpPasswordEncoder.getInstance();
    }
    @Bean
    public static BCryptPasswordEncoder encoder(){
        return new BCryptPasswordEncoder();
    }

    /**
     * Метод, устанавливает роли и пары логин/ пароль для них
     */
    @Autowired
    public void configureGlobalSecurity(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService).passwordEncoder(encoder());
    }

    /**
     * Метод, устанавливает степени защищенности для каждого из обработчика запросов из Rest контроллеров.
     */
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable().authorizeRequests().
                antMatchers("/bankServer/auth/**").hasRole("USER").
                antMatchers("/bankServer/bills/**").hasRole("USER").
                anyRequest().authenticated().and().
                httpBasic().authenticationEntryPoint(getBasicAuthEntryPoint());

    }

    /**
     * Возвращает объект, конфигурирующий ответ при провале проверки авторизации
     */
    @Bean
    public CustomBasicAuthenticationEntryPoint getBasicAuthEntryPoint() {
        return new CustomBasicAuthenticationEntryPoint();
    }

    }
