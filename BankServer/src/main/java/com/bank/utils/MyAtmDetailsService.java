package com.bank.utils;

import com.bank.dao.impl.AtmDaoImpl;
import com.bank.model.entities.AtmEntity;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class MyAtmDetailsService implements UserDetailsService {

    private AtmDaoImpl atmDao;

    public MyAtmDetailsService(AtmDaoImpl atmDao) {
        this.atmDao = atmDao;
    }

    public UserDetails loadUserByUsername(String name) {
       AtmEntity atmEntity = atmDao.findByAtmName(name);
       if (atmEntity == null){
           throw new UsernameNotFoundException(name);
       }
        return myUserDetails(atmEntity);
    }

    private UserDetails myUserDetails (AtmEntity atmEntity){
        return User.withUsername(atmEntity.getAtm_name()).password(atmEntity.getAtm_password()).roles(atmEntity.getAtm_role()).build();
    }
}
