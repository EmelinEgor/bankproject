package com.bank.utils;

import org.postgresql.util.PSQLException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;


@Component
public class CheckConnection {

    @Autowired
    private Environment environment;

    public boolean checkConnection (){
        boolean check;
        String url = environment.getProperty("spring.datasource.url");
        String username = environment.getProperty("spring.datasource.username");
        String password = environment.getProperty("spring.datasource.password");

        try(Connection connection = DriverManager.getConnection(url,username,password)){
            check = true;
        }
        catch (SQLException e) {
            check = false;
        }
        return check;
    }
}
