package com.bank.utils;

import com.bank.dao.interfaces.BillsDao;
import com.bank.dao.interfaces.UsersDao;
import com.bank.model.data.TokensStorage;
import com.bank.model.entities.UsersEntity;
import org.hibernate.HibernateException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Класс для валидации данных
 * <br> Поля: {@link #billsDao}, {@link #usersDao}
 * <br> Методы: {@link #checkUser(String, long, int)}, {@link #checkLimit(int, int)}, {@link #checkBalance(int, int)}
 *
 * @see com.bank.Application
 */
@Component
public class ValidHelper {

    /**
     * Класс выполняющий доступ к базе данных, таблице bills
     */
    private BillsDao billsDao;
    /**
     * Класс, осуществляющий доступ к базе данных, к наблице users
     */
    private UsersDao usersDao;

    private TokensStorage tokensStorage;

    /**
     * Создает новый объект
     *
     * @param billsDao инжекция зависимости, объект singleton
     * @param usersDao инжекция зависимости, объект singleton
     */
    public ValidHelper(BillsDao billsDao, UsersDao usersDao, TokensStorage tokensStorage) {
        this.billsDao = billsDao;
        this.usersDao = usersDao;
        this.tokensStorage = tokensStorage;
    }

    /**
     * метод, проверяющий что у данного пользователя есть данная карты и что есть такой пользователь
     *
     * @param fio        фамилия и имя пользователя
     * @param cardNumber номер карты
     * @return Результат проверки
     */
    public boolean checkUser(String fio, long cardNumber, int pin) throws HibernateException {
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        boolean check = false;
        UsersEntity userCards = usersDao.getUser(cardNumber);
        if (userCards == null) {
            return false;
        }
        if (encoder.matches(fio, userCards.getUser_name()) && encoder.matches(pin+"", userCards.getPin())) {
            check = true;
        }
        return check;
    }

    /**
     * Метод, промеряет, что количество снимаемых денег не выходит за лимит
     *
     * @param billNumber номер счета
     * @param amount     количсество снимаемой наличности
     * @return Результат проверки
     */
    public boolean checkLimit(int billNumber, int amount) throws HibernateException {
        return (amount <= billsDao.getBills(billNumber).get(0).getUser_limit());
    }

    /**
     * Метод, промеряет, что количество снимаемых денег не выходит за баланс
     *
     * @param billNumber номер счета
     * @param amount     количсество снимаемой наличности
     * @return Результат проверки
     */
    public boolean checkBalance(int billNumber, int amount) throws HibernateException {
        return (amount <= billsDao.getBills(billNumber).get(0).getBalance());
    }

    public boolean checkToken (String token){
        for (String x: tokensStorage.getTokens()){
            if (x.equals(token)){
                return true;
            }
        }
        return false;
    }
}
