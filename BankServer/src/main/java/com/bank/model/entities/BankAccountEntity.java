package com.bank.model.entities;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Класс сущность, отражающий таблицу bills из базы данных
 * <br> Поля: {@link #bill_number}, {@link #balance}, {@link #user_limit}, {@link #currency}
 * <br> Методы: {@link #getBill_number()}, {@link #setBill_number(int)}, {@link #getBalance()},
 * {@link #setBalance(int)}, {@link #getUser_limit()}, {@link #setUser_limit(int)},
 * {@link #getCurrency()}, {@link #setCurrency(String)}, {@link #toString()}.
 *
 * @see com.bank.Application
 */
@Component
@Entity
@Scope("prototype")
@Table(name = "bills", schema = "public")
public class BankAccountEntity implements Serializable {
    /**
     * Номер счета
     */
    @Id
    @Column(name = "bill_number", nullable = false)
    private int bill_number;
    /**
     * Баланс счета
     */
    @Basic
    @Column(name = "balance", nullable = false)
    private int balance;
    /**
     * Лимит снятия средств
     */
    @Basic
    @Column(name = "user_limit", nullable = false)
    private int user_limit;
    /**
     * Валюта счета
     */
    @Basic
    @Column(name = "currency", nullable = false)
    private String currency;

    /**
     * Возвращает значение bill_number, которое можно задать с помощью метода {@link #setBill_number(int)}
     *
     * @return Значение bill_number
     */
    public int getBill_number() {
        return bill_number;
    }

    /**
     * Задает значение bill_number, которое можно получить с помощью метода {@link #getBill_number()}
     *
     * @param bill_number новое значение bill_number
     */
    public void setBill_number(int bill_number) {
        this.bill_number = bill_number;
    }

    /**
     * Возвращает значение balance, которое можно задать с помощью метода {@link #setBalance(int)}
     *
     * @return Значение balance
     */
    public int getBalance() {
        return balance;
    }

    /**
     * Задает значение balance, которое можно получить с помощью метода {@link #getBalance()}
     *
     * @param balance новое значение balance
     */
    public void setBalance(int balance) {
        this.balance = balance;
    }

    /**
     * Возвращает значение user_limit, которое можно задать с помощью метода {@link #setUser_limit(int)}
     *
     * @return Значение user_limit
     */
    public int getUser_limit() {
        return user_limit;
    }

    /**
     * Задает значение user_limit, которое можно получить с помощью метода {@link #getUser_limit()}
     *
     * @param user_limit новое значение user_limit
     */
    public void setUser_limit(int user_limit) {
        this.user_limit = user_limit;
    }

    /**
     * Возвращает значение currency, которое можно задать с помощью метода {@link #setCurrency(String)}
     *
     * @return Значение currency
     */
    public String getCurrency() {
        return currency;
    }

    /**
     * Задает значение currency, которое можно получить с помощью метода {@link #getCurrency()}
     *
     * @param currency новое значение currency
     */
    public void setCurrency(String currency) {
        this.currency = currency;
    }

    /**
     * Строковое представление объекта
     *
     * @return Строка
     */
    @Override
    public String toString() {
        return "BillsEntity{" +
                "bill_number=" + bill_number +
                ", balance=" + balance +
                ", user_limit=" + user_limit +
                ", currency='" + currency + '\'' +
                '}';
    }
}
