package com.bank.model.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;

/**
 * Класс сущность, отражающий таблицу transactions из базы данных
 * <br> Поля: {@link #id}, {@link #bill_number}, {@link #date}, {@link #type}, {@link #sum}
 * <br> Методы: {@link #getId()}, {@link #setId(int)}, {@link #getBill_number()},
 * {@link #setBill_number(int)}, {@link #getDate()}, {@link #setDate(Timestamp)},
 * {@link #getType()}, {@link #setType(String)}, {@link #getSum()} ()}, {@link #setSum(int)}, {@link #toString()}.
 *
 * @see com.bank.Application
 */
@Entity
@Table(name = "transactions", schema = "public")
public class BankAccountStatementEntity implements Serializable {
    /**
     * Порядковый номер записи в таблице
     */
    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    /**
     * Номер счета
     */
    @Basic
    @Column(name = "bill_number", nullable = false)
    private int bill_number;
    /**
     * Дата транзакции
     */
    @Basic
    @Column(name = "date", nullable = false)
    private Timestamp date;
    /**
     * Тип транзакции
     */
    @Basic
    @Column(name = "type", nullable = false, length = 20)
    private String type;
    /**
     * Сумма транзакции
     */
    @Basic
    @Column(name = "sum", nullable = false)
    private int sum;

    /**
     * Возвращает значение id, которое можно задать с помощью метода {@link #setId(int)}
     *
     * @return Значение id
     */
    public int getId() {
        return id;
    }

    /**
     * Задает значение id, которое можно получить с помощью метода {@link #getId()}
     *
     * @param id новое значение id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Возвращает значение bill_number, которое можно задать с помощью метода {@link #setBill_number(int}
     *
     * @return Значение bill_number
     */
    public int getBill_number() {
        return bill_number;
    }

    /**
     * Задает значение bill_number, которое можно получить с помощью метода {@link #getBill_number()}
     *
     * @param bill_number новое значение bill_number
     */
    public void setBill_number(int bill_number) {
        this.bill_number = bill_number;
    }

    /**
     * Возвращает значение date, которое можно задать с помощью метода {@link #setDate(Timestamp)}
     *
     * @return Значение date
     */
    public Timestamp getDate() {
        return date;
    }

    /**
     * Задает значение date, которое можно получить с помощью метода {@link #getDate()}
     *
     * @param date новое значение date
     */
    public void setDate(Timestamp date) {
        this.date = date;
    }

    /**
     * Возвращает значение type, которое можно задать с помощью метода {@link #setType(String)}
     *
     * @return Значение type
     */
    public String getType() {
        return type;
    }

    /**
     * Задает значение type, которое можно получить с помощью метода {@link #getType()}
     *
     * @param type новое значение type
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * Возвращает значение sum, которое можно задать с помощью метода {@link #setSum(int)}
     *
     * @return Значение sum
     */
    public int getSum() {
        return sum;
    }

    /**
     * Задает значение sum, которое можно получить с помощью метода {@link #getSum()}
     *
     * @param sum новое значение sum
     */
    public void setSum(int sum) {
        this.sum = sum;
    }

    /**
     * Строковое представление объекта
     *
     * @return Строка
     */
    @Override
    public String toString() {
        return "BankAccountStatementEntity{" +
                "id=" + id +
                ", bill_number=" + bill_number +
                ", date=" + date +
                ", type='" + type + '\'' +
                ", sum=" + sum +
                '}';
    }
}

