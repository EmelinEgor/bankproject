package com.bank.model.entities;

import javax.persistence.*;

@Entity
@Table(name = "atm_list", schema = "public")
public class AtmEntity {

    @Id
    @Column(name = "atm_name", nullable = false)
    private String atm_name;
    @Basic
    @Column(name = "atm_password")
    private String atm_password;
    @Basic
    @Column(name = "atm_role")
    private String atm_role;

    public String getAtm_name() {
        return atm_name;
    }

    public void setAtm_name(String atm_name) {
        this.atm_name = atm_name;
    }

    public String getAtm_password() {
        return atm_password;
    }

    public void setAtm_password(String atm_password) {
        this.atm_password = atm_password;
    }

    public String getAtm_role() {
        return atm_role;
    }

    public void setAtm_role(String atm_role) {
        this.atm_role = atm_role;
    }

    @Override
    public String toString() {
        return "AtmEntity{" +
                "atm_name='" + atm_name + '\'' +
                ", atm_password='" + atm_password + '\'' +
                ", atm_role='" + atm_role + '\'' +
                '}';
    }
}
