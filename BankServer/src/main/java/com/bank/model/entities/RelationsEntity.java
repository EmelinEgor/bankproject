package com.bank.model.entities;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Класс сущность, отражающий таблицу relations из базы данных
 * <br> Поля: {@link #id}, {@link #card_number}, {@link #bill_number}
 * <br> Методы: {@link #getId()}, {@link #setId(int)},
 * {@link #getCard_number()}, {@link #setCard_number(long)},
 * {@link #getBill_number()}, {@link #setBill_number(int)}, {@link #toString()}.
 *
 * @see com.bank.Application
 */
@Entity
@Table(name = "relations", schema = "public")
public class RelationsEntity implements Serializable {
    /**
     * Номер записи в таблице
     */
    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private int id;
    /**
     * Номер карты
     */
    @Basic
    @Column(name = "card_number", nullable = false)
    private long card_number;
    /**
     * Номер счета
     */
    @Basic
    @Column(name = "bill_number", nullable = false)
    private int bill_number;

    /**
     * Возвращает значение id, которое можно задать с помощью метода {@link #setId(int)}
     *
     * @return Значение id
     */
    public int getId() {
        return id;
    }

    /**
     * Задает значение id, которое можно получить с помощью метода {@link #getId()}
     *
     * @param id новое значение id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Возвращает значение card_number, которое можно задать с помощью метода {@link #setCard_number(long)}
     *
     * @return Значение card_number
     */
    public long getCard_number() {
        return card_number;
    }

    /**
     * Задает значение card_number, которое можно получить с помощью метода {@link #getCard_number()}
     *
     * @param card_number новое значение card_number
     */
    public void setCard_number(long card_number) {
        this.card_number = card_number;
    }

    /**
     * Возвращает значение bill_number, которое можно задать с помощью метода {@link #setBill_number(int}
     *
     * @return Значение bill_number
     */
    public int getBill_number() {
        return bill_number;
    }

    /**
     * Задает значение bill_number, которое можно получить с помощью метода {@link #getBill_number()}
     *
     * @param bill_number новое значение bill_number
     */
    public void setBill_number(int bill_number) {
        this.bill_number = bill_number;
    }

    @Override
    public String toString() {
        return "RelationsEntity{" +
                "id=" + id +
                ", card_number=" + card_number +
                ", bill_number='" + bill_number + '\'' +
                '}';
    }
}
