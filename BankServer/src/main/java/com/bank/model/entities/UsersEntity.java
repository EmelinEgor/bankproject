package com.bank.model.entities;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Класс сущность, отражающий таблицу relations из базы данных
 * <br> Поля: {@link #card_number}, {@link #user_name}, {@link #pin}
 * <br> Методы: {@link #getCard_number()}, {@link #setCard_number(long)},
 * {@link #getUser_name()}, {@link #setUser_name(String)},
 * {@link #getPin()}, {@link #setPin(String)}, {@link #toString()}.
 *
 * @see com.bank.Application
 */
@Entity
@Table(name = "users", schema = "public")
public class UsersEntity implements Serializable {
    /**
     * Номер карты
     */
    @Id
    @Column(name = "card_number")
    private long card_number;
    /**
     * Фамилия и имя владельца карты
     */
    @Basic
    @Column(name = "user_name")
    private String user_name;
    /**
     * Пин-код карты
     */
    @Basic
    @Column(name = "pin")
    private String pin;

    /**
     * Возвращает значение card_number, которое можно задать с помощью метода {@link #setCard_number(long)}
     *
     * @return Значение card_number
     */
    public long getCard_number() {
        return card_number;
    }

    /**
     * Задает значение card_number, которое можно получить с помощью метода {@link #getCard_number()}
     *
     * @param card_number новое значение card_number
     */
    public void setCard_number(long card_number) {
        this.card_number = card_number;
    }

    /**
     * Возвращает значение user_name, которое можно задать с помощью метода {@link #setUser_name(String)}
     *
     * @return Значение user_name
     */
    public String getUser_name() {
        return user_name;
    }

    /**
     * Задает значение user_name, которое можно получить с помощью метода {@link #getUser_name()}
     *
     * @param user_name новое значение user_name
     */
    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    /**
     * Возвращает значение pin, которое можно задать с помощью метода {@link #setPin(String)}
     *
     * @return Значение pin
     */

    public String getPin() {
        return pin;
    }

    /**
     * Задает значение pin, которое можно получить с помощью метода {@link #getPin()}
     *
     * @param pin новое значение pin
     */
    public void setPin(String pin) {
        this.pin = pin;
    }


    @Override
    public String toString() {
        return "UsersEntity{" +
                "card_number=" + card_number +
                ", user_name='" + user_name + '\'' +
                ", pin='" + pin + '\'' +
                '}';
    }
}

