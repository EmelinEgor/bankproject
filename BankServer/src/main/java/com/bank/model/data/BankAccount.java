package com.bank.model.data;

import org.springframework.stereotype.Component;

/**
 * Класс для хранения данных о счете и количестве вносимой/нимаемой наличности
 * <br> Поля: {@link #currency}, {@link #cardNumber}, {@link #amount}.
 * <br> Методы: {@link #getCurrency()}, {@link #setCurrency(String)}, {@link #getCardNumber()},
 * {@link #setCardNumber(long)}, {@link #getAmount()}, {@link #setAmount(int)}, {@link #toString()}.
 *
 * @see com.bank.Application
 */
@Component
public class BankAccount {

        /**
         * Валюта счета
         */
        private String currency;
        /**
         * Номер карты
         */
        private long cardNumber;
        /**
         * Количество снимаемой/вносимой наличности
         */
        private int amount;

        private String token;
        /**
         * Создает новый объект
         */
        public BankAccount() {
        }

        /**
         * Возвращает значение currency, которое можно задать с помощью метода {@link #setCurrency(String)}
         *
         * @return Значение currency
         */
        public String getCurrency() {
            return currency;
        }

        /**
         * Задает значение currency, которое можно получить с помощью метода {@link #getCurrency()}
         *
         * @param currency новое значение currency
         */
        public void setCurrency(String currency) {
            this.currency = currency;
        }

        /**
         * Возвращает значение cardNumber, которое можно задать с помощью метода {@link #setCardNumber(long)}
         *
         * @return Значенте cardNumber
         */
        public long getCardNumber() {
            return cardNumber;
        }

        /**
         * Задает значение cardNumber, которое можно получить с помощью метода {@link #getCardNumber()}
         *
         * @param cardNumber новое значение cardNumber
         */
        public void setCardNumber(long cardNumber) {
            this.cardNumber = cardNumber;
        }

        /**
         * Возвращает значение amount, которое можно задать с помощью метода {@link #setAmount(int)}
         *
         * @return Значенте amount
         */
        public int getAmount() {
            return amount;
        }

        /**
         * Задает значение amount, которое можно получить с помощью метода {@link #getAmount()}
         *
         * @param amount новое значение amount
         */
        public void setAmount(int amount) {
            this.amount = amount;
        }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }


    }

