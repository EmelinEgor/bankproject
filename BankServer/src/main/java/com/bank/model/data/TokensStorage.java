package com.bank.model.data;

import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class TokensStorage {
     private List<String> tokens;

    public TokensStorage() {
        this.tokens = new ArrayList<String>();
    }

    public List<String> getTokens() {
        return tokens;
    }

    public void setTokens(String tokens) {
        this.tokens.add(tokens);
    }
}
