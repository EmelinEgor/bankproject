package com.bank.api;

import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.www.BasicAuthenticationEntryPoint;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Класс, формирует ответ в случае провала авторизации
 * <br> Методы: {@link #commence(HttpServletRequest, HttpServletResponse, AuthenticationException)}, {@link #afterPropertiesSet()}
 * @see com.bank.Application
 */
public class CustomBasicAuthenticationEntryPoint extends BasicAuthenticationEntryPoint {
    /**
     * Метод, в котором устанавливает ответ в случае провала авторизации
     * @param request запрос
     * @param response ответ
     * @param authException ошибка авторизации
     * @throws IOException возможное исключение
     * @throws ServletException возможное исключение
     */
    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException authException) throws IOException, ServletException {
        response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        response.addHeader("WWW-Authenticate", "Basic realm=" + getRealmName() + "");
        PrintWriter writer = response.getWriter();
        writer.println("HTTP Status 401 : " + authException.getMessage());
    }

    public void afterPropertiesSet() throws Exception {
        setRealmName("MY_TEST_REALM");
        super.afterPropertiesSet();
    }
}
