package com.bank.api;

import com.bank.adapter.service.interfaces.AuthService;
import com.bank.model.data.AuthorizationData;
import com.bank.model.data.TokensStorage;
import com.bank.utils.CheckConnection;
import org.springframework.context.annotation.Scope;
import org.springframework.http.*;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.security.SecureRandom;

/**
 * Является RestController. принимает вызовы, связанные с аутентификацией
 * <br> Поля: {@link #authService}
 * <br> Методы: {@link #auth(AuthorizationData)}
 *
 * @see com.bank.Application
 */
@RestController
@Scope("request")
public class AuthRest {
    /**
     * Сервис класс, выполняющий аутентификацию пользователя
     */
    private AuthService authService;

    private CheckConnection checkConnection;
    private TokensStorage tokensStorage;
    /**
     * создает новый объект
     *
     * @param authService инжекция зависимости, объект singleton
     */
    public AuthRest(AuthService authService, CheckConnection checkConnection, TokensStorage tokensStorage) {
        this.authService = authService;
        this.checkConnection = checkConnection;
        this.tokensStorage = tokensStorage;
    }

    /**
     * Обработчик запроса по адресу bankServer/auth
     *
     * @param authorizationData данные авторизации
     * @return Возвращает статус аутентификации (успех/не успех)
     */
    @RequestMapping(method = RequestMethod.POST, value = "bankServer/auth")
    private ResponseEntity auth(@RequestBody AuthorizationData authorizationData) {
        SecureRandom random = new SecureRandom();
        byte bytes[] = new byte[20];
        random.nextBytes(bytes);
        String token = bytes.toString();
        tokensStorage.setTokens(token);
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.TEXT_PLAIN);

        if (checkConnection.checkConnection()){
            return authService.getPin(authorizationData.getFio(), authorizationData.getCardNumber(), authorizationData.getPin()) ?
                    ResponseEntity.status(HttpStatus.ACCEPTED).headers(headers).body(token) :
                    ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        } else {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }
}
