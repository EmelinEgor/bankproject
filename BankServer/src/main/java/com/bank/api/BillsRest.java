package com.bank.api;

import com.bank.adapter.service.interfaces.BillsService;
import com.bank.model.data.AuthorizationData;
import com.bank.model.data.BankAccount;
import com.bank.model.data.TokensStorage;
import com.bank.utils.CheckConnection;
import com.bank.utils.ValidHelper;
import org.hibernate.HibernateException;
import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Является RestController. принимает вызовы, связанные с аутентификацией
 * <br> Поля: {@link #billsService}
 * <br> Методы: {@link #getCurrency(AuthorizationData)}, {@link #withdrawal(BankAccount)}, {@link #insertion(BankAccount)}, {@link #getBalance(BankAccount)},
 * {@link #getLimit(BankAccount)}, {@link #getBillHistory(BankAccount)}
 *
 * @see com.bank.Application
 */
@RestController
@Scope("session")
public class BillsRest {
    /**
     * Сервис класс, выполняющий логику работы со счетом
     */
    private BillsService billsService;

    private CheckConnection checkConnection;
    private ValidHelper validHelper;

    /**
     * Создает новый объект
     *
     * @param billsService инжекция зависимости, объект singleton
     */
    public BillsRest(BillsService billsService, CheckConnection checkConnection, ValidHelper validHelper) {
        this.billsService = billsService;
        this.checkConnection = checkConnection;
        this.validHelper = validHelper;
    }

    /**
     * Обработчик запроса по адресу bankServer/bills/getCurrency
     *
     * @param authorizationData данные авторизации
     * @return Возвращает список счетов
     */
    @RequestMapping(method = RequestMethod.POST, value = "bankServer/bills/getCurrency", produces = MediaType.APPLICATION_JSON_VALUE)
    private ResponseEntity getCurrency(@RequestBody AuthorizationData authorizationData) {
        if (validHelper.checkToken(authorizationData.getToken())) {
            if (checkConnection.checkConnection()) {
                return new ResponseEntity<>(billsService.getCurrency(authorizationData.getCardNumber()), HttpStatus.OK);
            } else {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
            }
        } else {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
        }
    }

    /**
     * Обработчик запроса по адресу bankServer/bills/withdrawal
     *
     * @param bankAccount данные, содержавщие необходимую информацию о счете
     * @return Возвращает статус выполнения операции
     */
    @RequestMapping(method = RequestMethod.POST, value = "bankServer/bills/withdrawal")
    private ResponseEntity withdrawal(@RequestBody BankAccount bankAccount) {
        if (validHelper.checkToken(bankAccount.getToken())) {
            if (checkConnection.checkConnection()) {
                return billsService.makeWithdrawal(billsService.getBill(bankAccount.getCardNumber(), bankAccount.getCurrency()), bankAccount.getAmount()) ?
                        ResponseEntity.status(HttpStatus.ACCEPTED).build() :
                        new ResponseEntity(HttpStatus.NO_CONTENT);
            } else {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
            }
        } else {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
        }
    }

    /**
     * Обработчик запроса по адресу bankServer/bills/insertion
     *
     * @param bankAccount данные, содержавщие необходимую информацию о счете
     * @return Возвращает статус выполнения операции
     */
    @RequestMapping(method = RequestMethod.POST, value = "bankServer/bills/insertion")
    private ResponseEntity insertion(@RequestBody BankAccount bankAccount) {
        if (validHelper.checkToken(bankAccount.getToken())) {
            if (checkConnection.checkConnection()) {

                return billsService.makeInsertion(billsService.getBill(bankAccount.getCardNumber(), bankAccount.getCurrency()), bankAccount.getAmount()) ?
                        ResponseEntity.status(HttpStatus.ACCEPTED).build() :
                        new ResponseEntity(HttpStatus.NO_CONTENT);
            } else {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
            }
        } else {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
        }
    }

    /**
     * Обработчик запроса по адресу bankServer/bills/getBalance
     *
     * @param bankAccount данные, содержавщие необходимую информацию о счете
     * @return Возвращает значение баланса
     */
    @RequestMapping(method = RequestMethod.POST, value = "bankServer/bills/getBalance")
    private ResponseEntity getBalance(@RequestBody BankAccount bankAccount) {
        if (validHelper.checkToken(bankAccount.getToken())) {
            if (checkConnection.checkConnection()) {
                int balance = billsService.getBalance(billsService.getBill(bankAccount.getCardNumber(), bankAccount.getCurrency()));
                if (balance != -10) {
                    return new ResponseEntity<>(balance, HttpStatus.OK);
                } else {
                    return new ResponseEntity<>(HttpStatus.NO_CONTENT);
                }

            } else {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
            }

        } else {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
        }
    }

    /**
     * Обработчик запроса по адресу bankServer/bills/getLimit
     *
     * @param bankAccount данные, содержавщие необходимую информацию о счете
     * @return Возвращает значение лимита
     */
    @RequestMapping(method = RequestMethod.POST, value = "bankServer/bills/getLimit")
    private ResponseEntity getLimit(@RequestBody BankAccount bankAccount) {
        if (validHelper.checkToken(bankAccount.getToken())) {
            if (checkConnection.checkConnection()) {
                int limit = billsService.getLimit(billsService.getBill(bankAccount.getCardNumber(), bankAccount.getCurrency()));
                if (limit != -10) {
                    return new ResponseEntity<>(limit, HttpStatus.OK);
                } else {
                    return new ResponseEntity<>(HttpStatus.NO_CONTENT);
                }

            } else {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
            }
        } else {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
        }
    }

    /**
     * Обработчик запроса по адресу bankServer/bills/getBillHistory
     *
     * @param bankAccount данные, содержавщие необходимую информацию о счете
     * @return Возвращает список всех транзакций
     */
    @RequestMapping(method = RequestMethod.POST, value = "bankServer/bills/getBillHistory")
    private ResponseEntity getBillHistory(@RequestBody BankAccount bankAccount) {
        if (validHelper.checkToken(bankAccount.getToken())) {
            if (checkConnection.checkConnection()) {
                try {
                    return new ResponseEntity<>(billsService.getBillHistory(billsService.getBill(bankAccount.getCardNumber(), bankAccount.getCurrency())), HttpStatus.OK);
                } catch (HibernateException e) {
                    return new ResponseEntity<>(HttpStatus.NO_CONTENT);
                }
            } else {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
            }
        } else {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
        }
    }
}
