package com.bank.adapter.service.interfaces;

public interface AuthService {

    boolean getPin(String fio, long cardNumber, int pin);
}
