package com.bank.adapter.service.impl;

import com.bank.adapter.service.interfaces.AuthService;
import com.bank.dao.interfaces.UsersDao;
import com.bank.utils.ValidHelper;
import org.hibernate.HibernateException;
import org.springframework.stereotype.Service;

/**
 * Данный класс служит для выолнения аутентификации пользователя. Применятеся в {@link com.bank.api.AuthRest}.
 * <br> Поля: {@link #validHelper}, {@link #usersDao}
 * <br> Методы: {@link #getPin(String, long, int)}
 *
 * @see com.bank.Application
 */
@Service
public class AuthServiceImpl implements AuthService {
    /**
     * Вспомогательный класс для валидации данных
     */
    private ValidHelper validHelper;
    /**
     * Класс, осуществляющий доступ к базе данных, к наблице users
     */
    private UsersDao usersDao;

    /**
     * Создвает новый объект
     *
     * @param validHelper инжекция зависимости, объект singleton
     * @param usersDao    инжекция зависимости, объект singleton
     */
    public AuthServiceImpl(ValidHelper validHelper, UsersDao usersDao) {
        this.validHelper = validHelper;
        this.usersDao = usersDao;
    }

    /**
     * Выполняет проверку на соответсвие введенных имени, фамилии, номера карты и пин-кода с хвранимым в базе данных
     *
     * @param fio        фамилия имя пользователя
     * @param cardNumber номер карты
     * @param pin        пин - код
     * @return возвращает true или false по результатам проверки
     */
    public boolean getPin(String fio, long cardNumber, int pin) {
        try {
            return validHelper.checkUser(fio, cardNumber, pin);
        } catch (HibernateException e) {
            return false;
        }
    }
}
