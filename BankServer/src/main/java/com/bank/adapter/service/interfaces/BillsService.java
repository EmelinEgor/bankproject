package com.bank.adapter.service.interfaces;

import com.bank.model.entities.BankAccountStatementEntity;

import java.util.List;

public interface BillsService {

    List<String> getCurrency(long cardNumber);

    int getBill(long cardNumber, String currency);

    boolean makeWithdrawal(int billNumber, int amount);

    boolean makeInsertion(int billNumber, int amount);

    int getBalance(int billNumber);

    int getLimit(int billNumber);

    List<BankAccountStatementEntity> getBillHistory(int billNumber);
}
