package com.bank.adapter.service.impl;

import com.bank.adapter.service.interfaces.BillsService;
import com.bank.dao.interfaces.BillsDao;
import com.bank.dao.interfaces.BillsHistoryDao;
import com.bank.dao.interfaces.RelationsDao;
import com.bank.model.entities.BankAccountEntity;
import com.bank.model.entities.BankAccountStatementEntity;
import com.bank.utils.ValidHelper;
import org.hibernate.HibernateException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * Данный класс служит для выолнения операций со счетом. Применятеся в {@link com.bank.api.BillsRest}.
 * <br> Поля: {@link #relationsDao}, {@link #billsHistoryDao}, {@link #billsDao}, {@link #validHelper }
 * <br> Методы: {@link #getCurrency(long)}, {@link #getBill(long, String)}, {@link #makeWithdrawal(int, int)}, {@link #makeInsertion(int, int)},
 * {@link #getBalance(int)}, {@link #getLimit(int)}, {@link #getBillHistory(int)}
 *
 * @see com.bank.Application
 */
@Service
@Transactional
public class BillsServiceImpl implements BillsService {

    /**
     * Класс выполняющий доступ к базе данных, таблице relations
     */
    private RelationsDao relationsDao;
    /**
     * Класс выполняющий доступ к базе данных, таблице transactions
     */
    private BillsHistoryDao billsHistoryDao;
    /**
     * Класс выполняющий доступ к базе данных, таблице bills
     */
    private BillsDao billsDao;
    /**
     * Вспомогательный класс для валидации данных
     */
    private ValidHelper validHelper;

    /**
     * Создает новый объект
     *
     * @param relationsDao    инжекция зависимости, объект singleton
     * @param billsHistoryDao инжекция зависимости, объект singleton
     * @param billsDao        инжекция зависимости, объект singleton
     * @param validHelper     инжекция зависимости, объект singleton
     */
    public BillsServiceImpl(RelationsDao relationsDao, BillsHistoryDao billsHistoryDao, BillsDao billsDao, ValidHelper validHelper) {
        this.relationsDao = relationsDao;
        this.billsHistoryDao = billsHistoryDao;
        this.billsDao = billsDao;
        this.validHelper = validHelper;
    }

    /**
     * Метод, ввыполныющий поиск всех счетов, привязанных к карте.
     *
     * @param cardNumber номер карты
     * @return Список все счетов в виде перечисления используемой в них валюты
     */
    public List<String> getCurrency(long cardNumber) {
        List<String> currencyList = new ArrayList<>();
        try {
            int[] billsList = relationsDao.getBillsNumber(cardNumber);
            List<BankAccountEntity> billsEntities = billsDao.getBills(billsList);

            for (BankAccountEntity bill : billsEntities) {
                currencyList.add(bill.getCurrency());
            }
            return currencyList;
        } catch (HibernateException e) {
            return currencyList;
        }
    }

    /**
     * Метод, получающий номер счета для текущей карты и выбранным пользователем счетов в виде валюты
     *
     * @param cardNumber номер карты
     * @param currency   выбранная валюта
     * @return Номер текущего счета
     */
    public int getBill(long cardNumber, String currency) {
        try{
            int[] billsList = relationsDao.getBillsNumber(cardNumber);
            return billsDao.getBill(currency, billsList);
        } catch (HibernateException e){
            return -10;
        }
    }

    /**
     * Метод, выполняющиый снятие средств
     *
     * @param billNumber номер счета, с которого происходит списание
     * @param amount     размер списания
     * @return возвращает true или false, говорящий об успехе операции
     */
    public boolean makeWithdrawal(int billNumber, int amount) {
        boolean makeOperation;
        try {
            if (validHelper.checkLimit(billNumber, amount)) {
                if (validHelper.checkBalance(billNumber, amount)) {
                    BankAccountEntity bankAccountEntity = billsDao.getBills(billNumber).get(0);
                    bankAccountEntity.setBalance(bankAccountEntity.getBalance() - amount);
                    bankAccountEntity.setUser_limit(bankAccountEntity.getUser_limit() - amount);
                    billsDao.updateBill(bankAccountEntity);
                    billsHistoryDao.createTransaction(billNumber, amount, "Withdrawal");
                    makeOperation = true;
                } else makeOperation = false;
            } else makeOperation = false;
        }catch (HibernateException e){
            makeOperation = false;
        }
        return makeOperation;
    }

    /**
     * метод, выполняющий пополнение счета
     *
     * @param billNumber номер текущего счета
     * @param amount     размер внесений
     */
    public boolean makeInsertion(int billNumber, int amount) {
        try{
            BankAccountEntity bankAccountEntity = billsDao.getBills(billNumber).get(0);
            bankAccountEntity.setBalance(bankAccountEntity.getBalance() + amount);
            billsDao.updateBill(bankAccountEntity);
            billsHistoryDao.createTransaction(billNumber, amount, "Insertion");
            return true;
        }catch (HibernateException e){
            return false;
        }

    }

    /**
     * Метод, возвращающий баланс на счете
     *
     * @param billNumber номер счета
     * @return Числовое значение баланса
     */
    public int getBalance(int billNumber) {
        try{
            return billsDao.getBills(billNumber).get(0).getBalance();
        }catch (HibernateException e){
            return -10;
        }

    }

    /**
     * Метод, возвращающий лимит снятия средств
     *
     * @param billNumber номер счета
     * @return Числовое значение лимита
     */
    public int getLimit(int billNumber) {
        try{
            return billsDao.getBills(billNumber).get(0).getUser_limit();
        }catch (HibernateException e){
            return -10;
        }

    }

    /**
     * метод, возвращаюий историю транзакций
     *
     * @param billNumber номер счета
     * @return Список всех транзакций
     */
    public List<BankAccountStatementEntity> getBillHistory(int billNumber) throws HibernateException {
        return billsHistoryDao.getHistory(billNumber);
    }
}
