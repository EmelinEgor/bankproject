package com.bank;

import com.bank.api.Console;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

/**
 * Класс, содержащий метод main, который создает контекст и вызывает консоль.
 *
 * @see com.bank.api.Console
 * @see com.bank.model.AuthorizationData
 * @see com.bank.model.BankAccount
 * @see com.bank.utils.JsonHelper
 * @see com.bank.utils.PropertyResolver
 * @see com.bank.utils.ValidHelper
 */
@SpringBootApplication
public class Application {

    public static void main(String[] args) throws Exception {
        ApplicationContext context = SpringApplication.run(Application.class, args);
        context.getBean(Console.class).auth();
    }
}
