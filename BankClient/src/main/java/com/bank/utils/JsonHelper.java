package com.bank.utils;

import com.bank.model.AuthorizationData;
import com.bank.model.BankAccount;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.stereotype.Component;


/**
 * Класс для сериализации объектов с помощью библиотеки Jackson. Применятеся в {@link com.bank.api.Console}.
 * <br> Методы: {@link #authorizationDataJson(AuthorizationData)}, {@link #bankAccountJson(BankAccount)}.
 *
 * @see com.bank.Application
 */
@Component
public class JsonHelper {

    /**
     * Сериализует объект authorizationData в строку для последующей передачи данных на сервер.
     *
     * @param authorizationData данные, используемые для авторизации (см. {@link AuthorizationData})
     * @return serializedString - сериализованный в строку объект authorizationData
     */
    public String authorizationDataJson(AuthorizationData authorizationData) {
        ObjectMapper mapper = new ObjectMapper();
        String serializedString = "";
        try {
            serializedString = mapper.writeValueAsString(authorizationData);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return serializedString;
    }

    /**
     * Сериализует объект bankAccount в строку для последующей передачи данных на сервер.
     *
     * @param bankAccount - данные, используемые для авторизации (см. {@link BankAccount})
     * @return serializedString - сериализованный в строку объект bankAccount
     */
    public String bankAccountJson(BankAccount bankAccount) {
        ObjectMapper mapper = new ObjectMapper();
        String serializedString = "";
        try {
            serializedString = mapper.writeValueAsString(bankAccount);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return serializedString;
    }
}
