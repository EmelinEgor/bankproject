package com.bank.utils;

import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.stereotype.Component;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

/**
 * Класс для конфигурации запросов, создаваемых в Console (см. {@link com.bank.api.Console}).
 * <br> Поля: {@link #plainCredentials}, {@link #strURLAuth}, {@link #strURLCurrency}, {@link #strURLWithdrawal}, {@link #strURLInsertion},
 * {@link #strURLBalance}, {@link #strURLLimit}, {@link #strURLHistory}.
 * <br> Методы: {@link #getPlainCredentials()}, {@link #getStrURLAuth()}, {@link #getStrURLCurrency()}, {@link #getStrURLWithdrawal()},
 * {@link #getStrURLInsertion()}, {@link #getStrURLBalance()}, {@link #getStrURLLimit()}, {@link #getStrURLHistory()}.
 *
 * @see com.bank.Application
 */
@Component
public class PropertyResolver {
    /**
     * Логин/пароль для Spring Security
     */
    private String plainCredentials;
    /**
     * Адрес для запроса авторизации
     */
    private String strURLAuth;
    /**
     * Адрес для запроса привязанных счетов
     */
    private String strURLCurrency;
    /**
     * Адрес для запроса снятия наличности
     */
    private String strURLWithdrawal;
    /**
     * Адрес для запроса внесения наличности
     */
    private String strURLInsertion;
    /**
     * Адрес для запроса получения баланса
     */
    private String strURLBalance;
    /**
     * Адрес для запроса получения лимита
     */
    private String strURLLimit;
    /**
     * Адрес для запроса истории операций
     */
    private String strURLHistory;

    /**
     * Считывает конфигурацию из application.properties и устанавливает адреса, логин/пароль
     */
    public PropertyResolver() {
        Properties property = new Properties();
        try (FileInputStream fis = new FileInputStream("BankClient/src/main/resources/config/application.properties")) {
            property.load(fis);
        } catch (IOException e) {
            System.err.println("ОШИБКА: Файл свойств отсуствует!");
        }

        plainCredentials = new String(Base64.encodeBase64(property.getProperty("cl.login").getBytes())) + ":" + property.getProperty("cl.password");
        strURLAuth = property.getProperty("strURL.auth");
        strURLCurrency = property.getProperty("strURL.currency");
        strURLWithdrawal = property.getProperty("strURL.withdrawal");
        strURLInsertion = property.getProperty("strURL.insertion");
        strURLBalance = property.getProperty("strURL.balance");
        strURLLimit = property.getProperty("strURL.limit");
        strURLHistory = property.getProperty("strURL.history");
    }

    /**
     * Возвращает значение plainCredentials
     *
     * @return Значение plainCredentials
     */
    public String getPlainCredentials() {
        return plainCredentials;
    }

    /**
     * Возвращает значение strURLAuth
     *
     * @return Значение strURLAuth
     */
    public String getStrURLAuth() {
        return strURLAuth;
    }

    /**
     * Возвращает значение strURLCurrency
     *
     * @return Значение strURLCurrency
     */
    public String getStrURLCurrency() {
        return strURLCurrency;
    }

    /**
     * Возвращает значение strURLWithdrawal
     *
     * @return Значение strURLWithdrawal
     */
    public String getStrURLWithdrawal() {
        return strURLWithdrawal;
    }

    /**
     * Возвращает значение strURLInsertion
     *
     * @return значение strURLInsertion
     */
    public String getStrURLInsertion() {
        return strURLInsertion;
    }

    /**
     * Возвращает значение strURLBalance
     *
     * @return Значение strURLBalance
     */
    public String getStrURLBalance() {
        return strURLBalance;
    }

    /**
     * Возвращает значение strURLLimit
     *
     * @return Значение strURLLimit
     */
    public String getStrURLLimit() {
        return strURLLimit;
    }

    /**
     * Возвращает значение strURLHistory
     *
     * @return Значение strURLHistory
     */
    public String getStrURLHistory() {
        return strURLHistory;
    }
}
