package com.bank.utils;

import com.bank.model.AuthorizationData;
import com.bank.model.BankAccount;
import org.springframework.stereotype.Component;

import java.util.List;


/**
 * Класс для выполнения валидации вводимых данных в {@link com.bank.api.Console}.
 * <br> Поля: {@link #authorizationData}, {@link #bankAccount}.
 * <br> Методы: {@link #checkCurrency(int)}, {@link #checkAuthInputData(String)}, {@link #checkSetBill(String, List)},
 * {@link #checkMenuChoice(String)}, {@link #checkAmount(String)}.
 *
 * @see com.bank.Application
 */
@Component
public class ValidHelper {

    /**
     * Вспомогательный класс для хранения данных для авторизации
     */
    private AuthorizationData authorizationData;
    /**
     * Вспомогательный класс для хранения данных о счёте
     */
    private BankAccount bankAccount;

    /**
     * Создает новый объект
     *
     * @param authorizationData инжекция зависимости, объект singleton
     * @param bankAccount       инжекция зависимости, объект singleton
     */
    public ValidHelper(AuthorizationData authorizationData, BankAccount bankAccount) {
        this.authorizationData = authorizationData;
        this.bankAccount = bankAccount;
    }

    /**
     * Метод слыжит для проверки корректности введенной суммы (кратность 100/200/500/1000/2000/5000)
     *
     * @param count введенная пользователем сумма
     * @return Возвращает true или false соответственно при правильном или неправильном вводе
     */
    private boolean checkCurrency(int count) {
        return count % 100 == 0 || count % 200 == 0 || count % 500 == 0
                || count % 1000 == 0 || count % 2000 == 0 || count % 5000 == 0;
    }

    /**
     * Метод служит для проверки правильности введенных пин-кода и номера карты.
     *
     * @param line введенная в консоль строка
     * @return Возвращает false или true соответственно при правильном или неправильном вводе
     */

    public boolean checkAuthInputData(String line) {
        boolean check = true;
        try {
            if (line.length() == 4) {
                int pinInt = Integer.parseInt(line);
                authorizationData.setPin(pinInt);
                check = false;
            } else if (line.length() == 16) {
                long card = Long.parseLong(line);
                authorizationData.setCardNumber(card);
                check = false;
            } else System.out.println("Неверное число введенных символов ");
        } catch (NumberFormatException e) {
            System.out.println("Неверный формат числа, допускаются только цифры. ");
        }
        return check;
    }

    /**
     * Метод проверяет правильность выбора счета, привязанного к карте
     *
     * @param line     введенная в консоль строка
     * @param response полученный от сервера список счетов
     * @return Возвращает false или true соответственно при правильном или неправильном вводе
     */
    public boolean checkSetBill(String line, List<String> response) {
        boolean check = true;
        try {

            if (line.length() == 1) {
                int bill = Integer.parseInt(line);
                if (bill < response.size()) {
                    bankAccount.setCurrency(response.get(bill));
                    check = false;
                }
            } else System.out.println("Неверный номер счета ");
        } catch (NumberFormatException e) {
            System.out.println("Неверный формат числа, допускаются только цифры. ");
        }
        return check;
    }

    /**
     * Метод проверяет какой выбран номер меню, и правильно ли осуществлен ввод
     *
     * @param line введенная в консоль строка
     * @return Возвращает false или true соответственно при правильном или неправильном вводе
     */
    public boolean checkMenuChoice(String line) {
        boolean check = true;
        try {
            if (line.length() == 1) {
                int point = Integer.parseInt(line);
                if (point < 7) {
                    check = false;
                }
            } else System.out.println("Неверный номер операции");
        } catch (NumberFormatException e) {
            System.out.println("Неверный формат числа, допускаются только цифры. ");
        }
        return check;
    }

    /**
     * Метод проверяет коректность введенной суммы для пополнения/снятия
     *
     * @param line введенная в консоль строка
     * @return Возвращает false или true соответственно при правильном или неправильном вводе
     */
    public boolean checkAmount(String line) {
        boolean check = true;
        try {
            int sum = Integer.parseInt(line);
            if (checkCurrency(sum)) {
                bankAccount.setAmount(sum);
                check = false;
            } else
                System.out.println("Невозможно выполнить операцию. Неверная введена сумма наличности");
        } catch (NumberFormatException e) {
            System.out.println("Неверный формат числа, допускаются только цифры. ");
        }
        return check;
    }
}
