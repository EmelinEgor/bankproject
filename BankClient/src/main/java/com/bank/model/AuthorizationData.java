package com.bank.model;

import org.springframework.stereotype.Component;

/**
 * Класс для хранения данных о карте и владельце для авторизации.
 * <br> Поля: {@link #fio}, {@link #cardNumber}, {@link #pin}.
 * <br> Методы: {@link #getFio()}, {@link #setFio(String)}, {@link #getCardNumber()},
 * {@link #setCardNumber(long)}, {@link #getPin()}, {@link #setPin(int)}, {@link #toString()}.
 *
 * @see com.bank.Application
 */
@Component
public class AuthorizationData {
    /**
     * Фамилия и имя владельца карты
     */
    private String fio;
    /**
     * Номер карты
     */
    private long cardNumber;
    /**
     * Пин-код карты
     */
    private int pin;

    private String token;

    /**
     * Создает новый объект
     */
    public AuthorizationData() {
    }

    /**
     * Возвращает значение fio, которое можно задать с помощью метода {@link #setFio(String)}
     *
     * @return Значение fio
     */
    public String getFio() {
        return fio;
    }

    /**
     * Задает значение fio, которое можно получить с помощью метода {@link #getFio()}
     *
     * @param fio новое значение fio
     */
    public void setFio(String fio) {
        this.fio = fio;
    }

    /**
     * Возвращает значение cardNumber, которое можно задать с помощью метода {@link #setCardNumber(long)}
     *
     * @return Значение cardNumber
     */
    public long getCardNumber() {
        return cardNumber;
    }

    /**
     * Задает значение cardNumber, которое можно получить с помощью метода {@link #getCardNumber()}
     *
     * @param cardNumber новое значение cardNumber
     */
    public void setCardNumber(long cardNumber) {
        this.cardNumber = cardNumber;
    }

    /**
     * Возвращает значение pin, которое можно задать с помощью метода {@link #setPin(int)}
     *
     * @return Значение pin
     */
    public int getPin() {
        return pin;
    }

    /**
     * Задает значение pin, которое можно получить с помощью метода {@link #getPin()}
     *
     * @param pin новое значение pin
     */
    public void setPin(int pin) {
        this.pin = pin;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    /**
     * Строковое представление объекта
     *
     * @return Строка
     */


    @Override
    public String toString() {
        return "AuthorizationData{" +
                "fio='" + fio + '\'' +
                ", cardNumber=" + cardNumber +
                ", pin=" + pin +
                ", token='" + token + '\'' +
                '}';
    }
}
