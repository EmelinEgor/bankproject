package com.bank.api;

import com.bank.model.BankAccount;

import com.bank.utils.JsonHelper;
import com.bank.model.AuthorizationData;
import com.bank.utils.PropertyResolver;
import com.bank.utils.ValidHelper;
import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.http.*;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

import java.util.*;

/**
 * Класс предоставляющий консоль для выполнения авторизации и операций со счетом.
 * <br> Поля: {@link #jsonHelper}, {@link #authorizationData}, {@link #bankAccount}, {@link #validHelper}, {@link #propertyResolver},
 * {@link #scanner}, {@link #restTemplate}.
 * <br> Методы: {@link #headers()}, {@link #auth()}, {@link #getCurrency()}, {@link #setBill(List)},
 * {@link #menu()}, {@link #withdrawal()}, {@link #insertion()}, {@link #getBalance()}, {@link #getLimit()}, {@link #getHistory()}.
 *
 * @see com.bank.Application
 */
@Component
public class Console {

    /**
     * Вспомогательный класс для сериализации объектов
     */
    private JsonHelper jsonHelper;
    /**
     * Вспомогательный класс для хранения данных для авторизации
     */
    private AuthorizationData authorizationData;
    /**
     * Вспомогательный класс для хранения данных о счете
     */
    private BankAccount bankAccount;
    /**
     * Вспомогательный класс для валидации вводимых данных
     */
    private ValidHelper validHelper;
    /**
     * Вспомогательный класс для конфигурации консоли
     */
    private PropertyResolver propertyResolver;
    /**
     * Вспомогательный класс для ввода в консоль
     */
    private Scanner scanner;
    /**
     * Вспомогательный класс для создания Rest запросов
     */
    private RestTemplate restTemplate;

    /**
     * Создает новый объект
     *
     * @param jsonHelper        инжекция зависимости, объект singleton
     * @param authorizationData инжекция зависимости, объект singleton
     * @param bankAccount       инжекция зависимости, объект singleton
     * @param validHelper       инжекция зависимости, объект singleton
     * @param propertyResolver  инжекция зависимости, объект singleton
     */
    public Console(JsonHelper jsonHelper, AuthorizationData authorizationData, BankAccount bankAccount, ValidHelper validHelper, PropertyResolver propertyResolver) {
        this.jsonHelper = jsonHelper;
        this.authorizationData = authorizationData;
        this.bankAccount = bankAccount;
        this.validHelper = validHelper;
        this.propertyResolver = propertyResolver;
        this.scanner = new Scanner(System.in);
        this.restTemplate = new RestTemplate();
    }

    /**
     * Метод для создания хейдера запроса с использованием BasicAuthentication
     *
     * @return header который устанавливается для всех запросов
     */
    private HttpHeaders headers() {
        String base64Credentials = new String(Base64.encodeBase64(propertyResolver.getPlainCredentials().getBytes()));
        HttpHeaders header = new HttpHeaders();
        header.add("Accept", MediaType.APPLICATION_JSON_VALUE);
        header.add("Authorization", "Basic " + base64Credentials);
        header.setContentType(MediaType.APPLICATION_JSON);
        return header;
    }

    /**
     * Метод, который позволяет ввести данные о карте и пройти аутенфикацию. В случае успеха вызывает {@link #getCurrency()}
     */
    public void auth() {
        boolean check = true;
        String line;

        System.out.println("Введите номерн карты (16 знаков): ");
        while (check) {
            line = scanner.nextLine();
            check = validHelper.checkAuthInputData(line);
        }

        System.out.println("Введите фамилию и имя: ");
        line = scanner.nextLine();
        authorizationData.setFio(line);

        System.out.println("Введите пин-код (4 знака): ");
        check = true;
        while (check) {
            line = scanner.nextLine();
            check = validHelper.checkAuthInputData(line);
        }

        try {
            System.out.println("Подтвердите ввод (y/n): ");
            if ("y".equals(scanner.nextLine())) {
                HttpEntity<String> requestBody = new HttpEntity<>(jsonHelper.authorizationDataJson(authorizationData), headers());
                ResponseEntity response = restTemplate.postForEntity(propertyResolver.getStrURLAuth(), requestBody, String.class);
                if (response.getStatusCodeValue() == 202) {
                    authorizationData.setToken(response.getBody().toString());
                    authorizationData.setFio("");
                    authorizationData.setPin(0);
                    bankAccount.setToken(response.getBody().toString());
                    getCurrency();
                }
            } else {
                auth();
            }
        } catch (HttpServerErrorException | NullPointerException e) {
            System.out.println("Серверная ошибка. Повторите ввод. ");
            auth();
        } catch (HttpClientErrorException e) {
            System.out.println("Авторизация не пройдена. Повторите ввод: ");
            auth();
        }

    }

    /**
     * Метод для получение счетов, привязанных к карте.
     */
    private void getCurrency() {
        try {
            HttpEntity<String> requestBody = new HttpEntity<String>(jsonHelper.authorizationDataJson(authorizationData), headers());
            List response = restTemplate.postForObject(propertyResolver.getStrURLCurrency(), requestBody, List.class);
            if (response.size() != 0) {
                for (int i = 0; i < response.size(); i++) {
                    System.out.println(i + ": " + response.get(i) + ";");
                }
                setBill(response);
            } else {
                System.out.println("Отсутствуют счета, привязанные к карте.");
            }
        } catch (HttpServerErrorException | NullPointerException e) {
            System.out.println("Серверная ошибка. Повторите ввод. ");
            auth();
        } catch (HttpClientErrorException e) {
            System.out.println("Авторизация не пройдена. Повторите ввод: ");
            auth();
        }
    }

    /**
     * Метод для установки счета, с которым будет работать пользователь.
     *
     * @param response список счетов, выводимый пользователю для выбора
     */
    private void setBill(List response) {
        bankAccount.setCardNumber(authorizationData.getCardNumber());
        System.out.println("Выбирете счет, с которым хотите работать: ");
        boolean check = true;
        while (check) {
            String line = scanner.nextLine();
            check = validHelper.checkSetBill(line, response);
        }
        menu();
    }

    /**
     * Метод, предоставляющий меню для выполнения операций.
     */
    private void menu() {
        System.out.println("Выберите действие, которое Вы хотите выполнить: ");
        System.out.println("0. Вернуться к выбору счета. ");
        System.out.println("1. Снять средства. ");
        System.out.println("2. Внести средства. ");
        System.out.println("3. Узнать баланс. ");
        System.out.println("4. Узнать суточный лимит. ");
        System.out.println("5. Получить выписку по операциям. ");
        System.out.println("6. Выход. ");

        int choice = -1;
        boolean check = true;

        while (check) {
            String line = scanner.nextLine();
            if (!(check = validHelper.checkMenuChoice(line))) {
                choice = Integer.parseInt(line);
            }
        }

        switch (choice) {
            case 0:
                getCurrency();
                break;
            case 1:
                withdrawal();
                break;
            case 2:
                insertion();
                break;
            case 3:
                getBalance();
                break;
            case 4:
                getLimit();
                break;
            case 5:
                getHistory();
                break;
            case 6:
                makeExit();
                break;
            default:
                System.out.println("Операция не выбранна.");
                menu();
        }
    }

    /**
     * Метод для выполнения операции снятия средств со счета.
     */
    private void withdrawal() {
        System.out.println("Введите сумму, которую хотите снять в пределах вашего баланса и суточного лимита.");
        System.out.println("Если вы их не знаете, то можете вернуться обратно в меню и выполнить запросы.");
        System.out.println("Продолжить (y/n): ");
        if ("y".equals(scanner.nextLine())) {
            System.out.println("Введите сумму (кратная 100, 200, 500, 1000, 2000, 5000): ");
            boolean check = true;

            while (check) {
                String line = scanner.nextLine();
                check = validHelper.checkAmount(line);
            }
            try {
                HttpEntity<String> requestBody = new HttpEntity<>(jsonHelper.bankAccountJson(bankAccount), headers());
                ResponseEntity entity = restTemplate.postForEntity(propertyResolver.getStrURLWithdrawal(), requestBody, Object.class);

                if (entity.getStatusCodeValue() == 202) {
                    System.out.println("Средства успешно сняты.");
                    System.out.println("Вернуться в меню (y)? ");
                    if (scanner.nextLine() != null) {
                        menu();
                    }
                } else if (entity.getStatusCodeValue() == 204) {
                    System.out.println("Ошибка при снятии. Проверьте баланс и лимит.");
                    menu();
                }

            } catch (HttpServerErrorException | NullPointerException e) {
                System.out.println("Серверная ошибка. Повторите ввод. ");
                auth();
            } catch (HttpClientErrorException e) {
                System.out.println("Авторизация не пройдена. Повторите ввод: ");
                auth();
            }
            menu();
        }

    }

    /**
     * Метод для выполнения операции внесения средств на счет.
     */
    private void insertion() {
        System.out.println("Введите сумму, которую хотите внести (кратная 100, 200, 500, 1000, 2000, 5000)");
        System.out.println("Введите сумму");
        boolean check = true;
        while (check) {
            String line = scanner.nextLine();
            check = validHelper.checkAmount(line);
        }
        try {
            HttpEntity<String> requestBody = new HttpEntity<>(jsonHelper.bankAccountJson(bankAccount), headers());
            ResponseEntity entity = restTemplate.postForEntity(propertyResolver.getStrURLInsertion(), requestBody, Object.class);

            if (entity.getStatusCodeValue() == 202) {
                System.out.println("Средства успешно внесены.");
                System.out.println("Вернуться в меню (y)? ");
                if (scanner.nextLine() != null) {
                    menu();
                }
            } else if (entity.getStatusCodeValue() == 204){
                System.out.println("Внутренняя ошибка сервера.");
                menu();
            }
        } catch (HttpServerErrorException | NullPointerException e) {
            System.out.println("Серверная ошибка. Повторите ввод. ");
            auth();
        } catch (HttpClientErrorException e) {
            System.out.println("Авторизация не пройдена. Повторите ввод: ");
            auth();
        }
        menu();
    }

    /**
     * Метод для выполнения операции получения баланса.
     */
    private void getBalance() {
        System.out.println("Ваш баланс составляет: ");
        try {
            HttpEntity<String> requestBody = new HttpEntity<>(jsonHelper.bankAccountJson(bankAccount), headers());
            Integer balance = restTemplate.postForObject(propertyResolver.getStrURLBalance(), requestBody, Integer.class);

            System.out.println(balance + " " + bankAccount.getCurrency());
        } catch (HttpServerErrorException | NullPointerException e) {
            System.out.println("Серверная ошибка. Повторите ввод. ");
            auth();
        } catch (HttpClientErrorException e) {
            System.out.println("Авторизация не пройдена. Повторите ввод: ");
            auth();
        }
        System.out.println("Вернуться в меню (y)? ");
        if (scanner.nextLine() != null) {
            menu();
        }
    }

    /**
     * Метод для выполнения операции получения лимита на снятие
     */
    private void getLimit() {
        System.out.println("Ваш лимит составляет: ");
        try {
            HttpEntity<String> requestBody = new HttpEntity<>(jsonHelper.bankAccountJson(bankAccount), headers());
            Integer limit = restTemplate.postForObject(propertyResolver.getStrURLLimit(), requestBody, Integer.class);

            System.out.println(limit + " " + bankAccount.getCurrency());
        } catch (HttpServerErrorException | NullPointerException e) {
            System.out.println("Серверная ошибка. Повторите ввод. ");
            auth();
        } catch (HttpClientErrorException e) {
            System.out.println("Авторизация не пройдена. Повторите ввод: ");
            auth();
        }
        System.out.println("Вернуться в меню (y)? ");
        if (scanner.nextLine() != null) {
            menu();
        }
    }

    /**
     * Метод для выполнения операции получения списка всех транзакций для данного счета.
     */
    private void getHistory() {
        System.out.println("История ваших операций: ");
        try {
            HttpEntity<String> requestBody = new HttpEntity<>(jsonHelper.bankAccountJson(bankAccount), headers());
            List bankAccountStatements = restTemplate.postForObject(propertyResolver.getStrURLHistory(), requestBody, List.class);


            if (bankAccountStatements != null) {
                for (int i = 0; i < bankAccountStatements.size(); i++)
                    System.out.println(bankAccountStatements.get(i));
            } else {
                System.out.println("Операций не было.");
            }
        } catch (HttpServerErrorException | NullPointerException e) {
            System.out.println("Серверная ошибка. Повторите ввод. ");
            auth();
        } catch (HttpClientErrorException e) {
            System.out.println("Авторизация не пройдена. Повторите ввод: ");
            auth();
        }
        System.out.println("Вернуться в меню (y)? ");
        if (scanner.nextLine() != null) {
            menu();
        }
    }

    /**
     * Метод для выхода из консоли.
     */
    private void makeExit() {
        auth();
    }
}


